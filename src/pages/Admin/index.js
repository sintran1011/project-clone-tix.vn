import React from "react";
import AdminDashboard from "../../HOCs/Layout/adminDashboard";

const AdminPage = () => {
  return (
    <AdminDashboard>
      <div
        className="h-screen w-full"
        style={{
          backgroundImage: `url(${"https://tix.vn/app/assets/img/icons/bg2.jpg"})`,
         
        }}
      >
        <h1 className="text-white text-center text-6xl top-44 relative font-bold">
          Chào mừng đến với admin page
        </h1>
      </div>
    </AdminDashboard>
  );
};

export default AdminPage;
