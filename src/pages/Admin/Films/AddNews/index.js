import React, { useState } from "react";
import AdminPage from "../../../../HOCs/Layout/adminDashboard";
import {
  Form,
  Input,
  Button,
  DatePicker,
  InputNumber,
  Switch,
  Upload,
  message,
  Image,
} from "antd";
import { InboxOutlined } from "@ant-design/icons";
import { useFormik } from "formik";
import * as yup from "yup";
import moment from "moment";
import Modal from "antd/lib/modal/Modal";
import "./style.css";
import { themPhimUploadHinh } from "../../../../services/QuanLyPhim";
import { useHistory } from "react-router-dom";
import { fetchMovieList } from "../../../../store/actions/movielist";
import { useDispatch } from "react-redux";

const { Dragger } = Upload;

const schema = yup.object().shape({
  tenPhim: yup.string().required("Vui lòng nhập tên phim"),
  maPhim: yup.string().required("Vui lòng nhập mã phim"),
  trailer: yup.string().required("Vui lòng chèn link trailer"),
  moTa: yup.string().required("Vui lòng nhập mô tả"),
  ngayKhoiChieu: yup.string().required("Vui lòng chọn ngày khởi chiếu"),
  danhGia: yup.string().required("Vui lòng đánh giá"),
});

const AddNews = () => {
  const [previewVisible, setPreviewVisible] = useState(false);
  const [previewImage, setPreviewImage] = useState("");

  const history = useHistory();
  const dispatch = useDispatch();

  const {
    handleBlur,
    handleChange,
    values,
    setFieldValue,
    touched,
    errors,
    setTouched,
    isValid,
  } = useFormik({
    initialValues: {
      maPhim: "",
      tenPhim: "",
      trailer: "",
      moTa: "",
      maNhom: "GP01",
      ngayKhoiChieu: "",
      sapChieu: "",
      dangChieu: "",
      hot: "",
      danhGia: "",
      hinhAnh: {},
    },
    validationSchema: schema,
    validateOnMount: true,
  });

  const handleSubmit = async (e) => {
    e.preventDefault();
    setTouched({
      maPhim: true,
      tenPhim: true,
      trailer: true,
      moTa: true,
      ngayKhoiChieu: true,
      danhGia: true,
    });

    let formData = new FormData();
    for (const key in values) {
      if (key !== "hinhAnh") {
        formData.append(key, values[key]);
      } else {
        formData.append("hinhAnh", values.hinhAnh, values.hinhAnh.name);
      }
    }

    if (!isValid) {
      return;
    }

    try {
      const res = await themPhimUploadHinh(formData);

      console.log("ThêmPhim", res.data.content);
      dispatch(fetchMovieList());
      message.success("Thêm phim thành công");
      history.push("/Admin/Films");
    } catch (err) {
      console.log("err", err);
    }
  };

  //   Xử lí chức năng upload
  const handleUpload = (file) => {
    let fileData = file.file;

    // status chỉ có khi thực hiện 1 action như remove,bt không có
    const status = file.file.status;

    if (status !== "removed") {
      message.success("Upload thành công");
    }
    if (status === "removed") {
      message.warning("Remove file");
    }
    console.log(file.file);
    setFieldValue("hinhAnh", fileData);
  };

  // xem trước hình trước khi upload
  const handlePreview = (file) => {
    setPreviewVisible(true);
    if (file) {
      // thumbUrl được tạo bởi ant antd, console để xem
      setPreviewImage(file.thumbUrl);
    }
    console.log(file);
    console.log("status", file.status);
  };

  const handleCancel = () => {
    setPreviewVisible(false);
  };

  return (
    <AdminPage>
      <Form
        onSubmitCapture={handleSubmit}
        className="w-1/2 mx-auto bg-gray-200 p-6"
        labelCol={{ span: 4 }}
        layout="horizontal"
      >
        <h3 className="text-center font-bold text-3xl mb-3">Chi tiết phim</h3>
        <Form.Item label="Mã phim">
          <InputNumber
            name="maPhim"
            onBlur={handleBlur}
            onChange={(val) => setFieldValue("maPhim", val)}
          />
          {touched.maPhim && <p className="text-red-700">{errors.maPhim}</p>}
        </Form.Item>
        <Form.Item label="Tên phim">
          <Input name="tenPhim" onBlur={handleBlur} onChange={handleChange} />
          {touched.tenPhim && <p className="text-red-700">{errors.tenPhim}</p>}
        </Form.Item>
        <Form.Item label="Trailer">
          <Input name="trailer" onBlur={handleBlur} onChange={handleChange} />
          {touched.trailer && <p className="text-red-700">{errors.trailer}</p>}
        </Form.Item>
        <Form.Item label="Mô tả">
          <Input name="moTa" onBlur={handleBlur} onChange={handleChange} />
          {touched.moTa && <p className="text-red-700">{errors.moTa}</p>}
        </Form.Item>
        <Form.Item label="Ngày khởi chiếu">
          <DatePicker
            format="DD/MM/YYYY"
            name="ngayKhoiChieu"
            onChange={(val) => {
              setFieldValue("ngayKhoiChieu", moment(val).format("DD/MM/YYYY"));
            }}
          />
          {touched.ngayKhoiChieu && (
            <p className="text-red-700">{errors.ngayKhoiChieu}</p>
          )}
        </Form.Item>
        <Form.Item label="Sắp chiếu" valuePropName="checked">
          <Switch onChange={(val) => setFieldValue("sapChieu", val)} />
        </Form.Item>
        <Form.Item label="Đang chiếu" valuePropName="checked">
          <Switch onChange={(val) => setFieldValue("dangChieu", val)} />
        </Form.Item>
        <Form.Item label="Hot" valuePropName="checked">
          <Switch onChange={(val) => setFieldValue("hot", val)} />
        </Form.Item>
        <Form.Item label="Đánh giá">
          <InputNumber
            min={1}
            max={10}
            onChange={(val) => {
              setFieldValue("danhGia", val);
            }}
          />
          {touched.danhGia && <p className="text-red-700">{errors.danhGia}</p>}
        </Form.Item>
        <Dragger
          listType="picture"
          onPreview={handlePreview}
          onChange={handleUpload}
          beforeUpload={() => false}
        >
          <p className="ant-upload-drag-icon">
            <InboxOutlined />
          </p>
          <p className="ant-upload-text">
            Click or drag file to this area to upload
          </p>
        </Dragger>

        <Modal visible={previewVisible} footer={null} onCancel={handleCancel}>
          <div className="AddNews_Custom_Image">
            <Image alt="uploadImage" preview={false} src={previewImage} />
          </div>
        </Modal>
        <Button
          htmlType="submit"
          className="mx-auto block rounded-full bg-blue-500 text-white font-bold mt-5"
        >
          <p>Thêm phim</p>
        </Button>
      </Form>
    </AdminPage>
  );
};

export default AddNews;
