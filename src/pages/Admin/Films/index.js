import React, { Fragment, useState } from "react";
import { Button, Table, Tag } from "antd";
import { Input } from "antd";
import {
  CalendarOutlined,
  DeleteOutlined,
  EditOutlined,
  SearchOutlined,
} from "@ant-design/icons";
import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";
import { deleteFilm, fetchMovieList } from "../../../store/actions/movielist";
import { NavLink } from "react-router-dom";
import AdminDashboard from "../../../HOCs/Layout/adminDashboard";

const { Search } = Input;

const Films = () => {
  const dispatch = useDispatch();

  const { moviesList } = useSelector((state) => state.MovieList);

  useEffect(() => {
    dispatch(fetchMovieList());
  }, [dispatch]);

  const data = moviesList;

  // chức năng search

  const onSearch = (tenphim) => {
    dispatch(fetchMovieList(tenphim));
  };

  const columns = [
    {
      title: "Mã Phim",
      dataIndex: "maPhim",
      sorter: (a, b) => a.maPhim - b.maPhim,
      sortDirections: ["descend"],
      width: "10%",
    },
    {
      title: "Hình ảnh",
      dataIndex: "hinhAnh",
      width: "10%",
      render: (text, film) => {
        return (
          <img
            className="block rounded-xl"
            src={film.hinhAnh}
            alt={film.tenPhim}
            style={{ width: 50, height: 50 }}
            onError={(e) => {
              e.target.onerror = null;
              e.target.src = `http://picsum.photo/${Math.floor(
                Math.random() * 500
              )}`;
            }}
          />
        );
      },
    },
    {
      title: "Tên Phim",
      dataIndex: "tenPhim",
      width: "20%",
      defaultSortOrder: "descend",
    },
    {
      title: "Mô tả",
      dataIndex: "moTa",
      render: (text, film) => {
        return (
          <Fragment>
            {film.moTa.length > 50
              ? film.moTa.substr(0, 100) + " ..."
              : film.moTa}
          </Fragment>
        );
      },
      width: "30%",
    },
    {
      title: "Trạng thái",
      dataIndex: "maPhim",
      render: (text, film) => {
        return (
          <Fragment>
            {film.dangChieu === true ? (
              <Tag className="rounded-xl my-1" color="magenta">
                Đang chiếu
              </Tag>
            ) : null}
            {film.sapChieu === true ? (
              <Tag className="rounded-xl my-1" color="cyan">
                Sắp chiếu
              </Tag>
            ) : null}
            {film.hot === true ? (
              <p className="my-1">
                <Tag className="rounded-xl my-1" color="volcano">
                  Hot
                </Tag>
              </p>
            ) : null}
          </Fragment>
        );
      },
      width: "15%",
    },
    {
      title: "Hành động",
      dataIndex: "maPhim",
      render: (text, film) => {
        return (
          <Fragment className="flex align-baseline">
            <NavLink
              className=" text-blue-600 mr-2 p-1 text-2xl"
              to={`/Admin/Films/Edit/${film.maPhim}`}
            >
              <EditOutlined />
            </NavLink>
            <NavLink
              className=" text-pink-500 mr-2 p-1 text-2xl"
              to={`/Admin/Films/Showtimes/${film.maPhim}`}
            >
              <CalendarOutlined />
            </NavLink>
            <span
              onClick={() => {
                if (
                  window.confirm(
                    "Bạn có chắc muốn xóa phim " + film.tenPhim + "?"
                  )
                ) {
                  dispatch(deleteFilm(film.maPhim));
                }
              }}
              className="text-red-700 text-opacity-50 p-1 text-2xl cursor-pointer hover:text-opacity-100"
              to="/"
            >
              <DeleteOutlined />
            </span>
          </Fragment>
        );
      },
    },
  ];

  //   ant desgin props table phải dùng boolean dạng state
  const [stateFalse] = useState(false);

  const configPagination = {
    pagination: {
      position: [`bottomCenter`],
      responsive: "true",
    },
  };

  return (
    <AdminDashboard>
      <div className="container bg-gray-200 p-5 rounded-3xl">
        <h3 className="text-3xl font-bold">Quản lý phim</h3>
        <NavLink to="/Admin/Films/AddNews">
          <Button className="block mt-4 font-semibold rounded">
            Thêm phim
          </Button>
        </NavLink>
        <Search
          loading={false}
          className="py-5"
          style={{ width: 400 }}
          placeholder="Vui lòng nhập thông tin phim để tìm"
          onSearch={onSearch}
          enterButton={<SearchOutlined />}
        />
        <Table
          rowKey={"maPhim"}
          showSorterTooltip={stateFalse}
          columns={columns}
          dataSource={data}
          // onChange={onChange}
          {...configPagination}
        />
      </div>
    </AdminDashboard>
  );
};

export default Films;
