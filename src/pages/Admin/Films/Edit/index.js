import React, { useEffect, useState } from "react";
import AdminPage from "../../../../HOCs/Layout/adminDashboard";
import {
  Form,
  Input,
  Button,
  DatePicker,
  InputNumber,
  Switch,
  Upload,
  message,
  Image,
} from "antd";
import { InboxOutlined } from "@ant-design/icons";
import { useFormik } from "formik";
import moment from "moment";
import Modal from "antd/lib/modal/Modal";
import "./style.css";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, useParams } from "react-router-dom";
import { editFilm, fetchInfoFilm } from "../../../../store/actions/movielist";

import { GROUPID } from "../../../../util/config";

const { Dragger } = Upload;

const Edit = () => {
  // state
  const [previewVisible, setPreviewVisible] = useState(false);
  const [previewImage, setPreviewImage] = useState("");

  // hook
  const dispatch = useDispatch();
  const { id } = useParams();
  const { infoFilm } = useSelector((state) => state.MovieList);
  const history = useHistory();

  useEffect(() => {
    dispatch(fetchInfoFilm(id));
  }, [dispatch, id]);

  const { handleBlur, handleChange, values, setFieldValue, touched, errors } =
    useFormik({
      enableReinitialize: true,
      initialValues: {
        maPhim: infoFilm?.maPhim,
        tenPhim: infoFilm?.tenPhim,
        trailer: infoFilm?.trailer,
        moTa: infoFilm?.moTa,
        ngayKhoiChieu: infoFilm?.ngayKhoiChieu,
        sapChieu: infoFilm?.sapChieu,
        dangChieu: infoFilm?.dangChieu,
        hot: infoFilm?.hot,
        danhGia: infoFilm?.danhGia,
        hinhAnh: null,
        maNhom: GROUPID,
      },
    });

  const handleSubmit = (e) => {
    e.preventDefault();

    let formData = new FormData();

    for (const key in values) {
      if (key !== "hinhAnh") {
        formData.append(key, values[key]);
      } else {
        if (values.hinhAnh !== null) {
          formData.append("hinhAnh", values.hinhAnh, values.hinhAnh.name);
        }
      }
    }

    console.log(values);

    dispatch(editFilm(formData));
    history.push("/Admin/Films");
  };

  //   Xử lí chức năng upload
  const handleUpload = (file) => {
    let fileData = file.file;
    setPreviewImage(true);
    // status chỉ có khi thực hiện 1 action như remove,bt không có
    const status = file.file.status;

    if (status !== "removed") {
      message.success("Upload thành công");
    }
    if (status === "removed") {
      message.warning("Remove file");
    }
    console.log(file.file);
    setFieldValue("hinhAnh", fileData);
  };

  // xem trước hình trước khi upload
  const handlePreview = (file) => {
    setPreviewVisible(true);
    if (file) {
      // thumbUrl được tạo bởi ant antd, console để xem
      setPreviewImage(file.thumbUrl);
    }
    console.log(file);
    console.log("status", file.status);
  };

  const handleCancel = () => {
    setPreviewVisible(false);
  };

  return (
    <AdminPage>
      <Form
        onSubmitCapture={handleSubmit}
        className="w-1/2 mx-auto bg-gray-200 p-6"
        labelCol={{ span: 4 }}
        layout="horizontal"
      >
        <h3 className="text-center font-bold text-3xl mb-3">Chi tiết phim</h3>

        <Form.Item label="Tên phim">
          <Input
            value={values.tenPhim}
            name="tenPhim"
            onBlur={handleBlur}
            onChange={handleChange}
          />
          {touched.tenPhim && <p className="text-red-700">{errors.tenPhim}</p>}
        </Form.Item>
        <Form.Item label="Trailer">
          <Input
            value={values.trailer}
            name="trailer"
            onBlur={handleBlur}
            onChange={handleChange}
          />
          {touched.trailer && <p className="text-red-700">{errors.trailer}</p>}
        </Form.Item>
        <Form.Item label="Mô tả">
          <Input
            value={values.moTa}
            name="moTa"
            onBlur={handleBlur}
            onChange={handleChange}
          />
          {touched.moTa && <p className="text-red-700">{errors.moTa}</p>}
        </Form.Item>
        <Form.Item label="Ngày khởi chiếu">
          <DatePicker
            allowClear={false}
            format="DD/MM/YYYY"
            value={moment(values.ngayKhoiChieu)}
            onChange={(val) => {
              setFieldValue("ngayKhoiChieu", moment(val));
            }}
          />
          {touched.ngayKhoiChieu && (
            <p className="text-red-700">{errors.ngayKhoiChieu}</p>
          )}
        </Form.Item>
        <Form.Item label="Sắp chiếu">
          <Switch
            checked={values.sapChieu}
            onChange={(val) => setFieldValue("sapChieu", val)}
          />
        </Form.Item>
        <Form.Item label="Đang chiếu">
          <Switch
            checked={values.dangChieu}
            onChange={(val) => setFieldValue("dangChieu", val)}
          />
        </Form.Item>
        <Form.Item label="Hot">
          <Switch
            checked={values.hot}
            onChange={(val) => setFieldValue("hot", val)}
          />
        </Form.Item>
        <Form.Item label="Đánh giá">
          <InputNumber
            value={values.danhGia}
            min={1}
            max={10}
            onChange={(val) => {
              setFieldValue("danhGia", val);
            }}
          />
          {touched.danhGia && <p className="text-red-700">{errors.danhGia}</p>}
        </Form.Item>

        <Dragger
          listType="picture"
          onPreview={handlePreview}
          onChange={handleUpload}
          beforeUpload={() => false}
        >
          <p className="ant-upload-drag-icon">
            <InboxOutlined />
          </p>
          <p className="ant-upload-text">
            Click or drag file to this area to upload
          </p>
        </Dragger>

        {previewImage === "" ? (
          <Image
            width={100}
            src={previewImage === "" ? infoFilm.hinhAnh : null}
          />
        ) : null}

        <Modal visible={previewVisible} footer={null} onCancel={handleCancel}>
          <div className="Edit_Custom_Image">
            <Image alt="uploadImage" preview={false} src={previewImage} />
          </div>
        </Modal>
        <Button
          htmlType="submit"
          className="mx-auto block rounded-full bg-blue-500 text-white font-bold mt-5"
        >
          <p>Cập nhật</p>
        </Button>
      </Form>
    </AdminPage>
  );
};

export default Edit;
