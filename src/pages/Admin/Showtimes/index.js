import React, { useEffect, useState } from "react";
import AdminDashboard from "../../../HOCs/Layout/adminDashboard";
import {
  Form,
  Button,
  DatePicker,
  InputNumber,
  Select,
  Image,
  Input,
} from "antd";

import style from "./style.module.css";
import { useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { fetchInfoFilm } from "../../../store/actions/movielist";
import {
  fetchCinema,
  fetchCinemaList,
  makeShowtime,
} from "../../../store/actions/cinema";
import { useFormik } from "formik";
import moment from "moment";

const Showtimes = () => {
  const { id } = useParams();
  const dispatch = useDispatch();

  const [maHeThongRap, setMaHeThongRap] = useState("");
  const [cumRap, setCumRap] = useState(null);

  const { setFieldValue, values } = useFormik({
    initialValues: {
      maPhim: id,
      ngayChieuGioChieu: "",
      maRap: "",
      giaVe: "",
    },
  });

  const { infoFilm } = useSelector((state) => state.MovieList);
  const { cinemaGroup } = useSelector((state) => state.Cinema);
  const { cinemaDetail } = useSelector((state) => state.Cinema);

  useEffect(() => {
    dispatch(fetchInfoFilm(id));
  }, [dispatch, id]);

  useEffect(() => {
    dispatch(fetchCinema());
  }, [dispatch]);

  useEffect(() => {
    dispatch(fetchCinemaList(maHeThongRap));
  }, [dispatch, maHeThongRap]);

  // set mã hệ thống rạp để dispatch và clear input cụm rạp
  const handleChangeValue = (value) => {
    setMaHeThongRap(value);
    setCumRap(null);
  };

  const ok = (value) => {
    setFieldValue(
      "ngayChieuGioChieu",
      moment(value).format("DD/MM/YYYY hh:mm:ss A")
    );
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    console.log(values);
    dispatch(makeShowtime(values));
  };

  return (
    <AdminDashboard>
      <Form
        onSubmitCapture={handleSubmit}
        className={`${style["centered"]} bg-white rounded-2xl shadow-2xl`}
        labelCol={{ span: 7 }}
        wrapperCol={{ span: 12 }}
        layout="horizontal"
      >
        <h3 className="my-5 text-3xl font-bold text-center">Tạo lịch chiếu</h3>
        <Form.Item label="Hình ảnh">
          <Image
            preview={false}
            className="mx-auto block"
            width={"50%"}
            src={infoFilm.hinhAnh}
          />
        </Form.Item>
        <Form.Item label="Tên phim">
          <Input disabled value={infoFilm.tenPhim} />
        </Form.Item>
        <Form.Item label="Hệ thống rạp">
          <Select
            onChange={(value) => {
              handleChangeValue(value);
            }}
            placeholder="Vui lòng chọn hệ thống rạp"
            options={cinemaGroup.map((item) => ({
              label: item.tenHeThongRap,
              value: item.maHeThongRap,
            }))}
          />
        </Form.Item>
        <Form.Item label="Rạp">
          <Select
            value={cumRap}
            onChange={setCumRap}
            onSelect={(value) => setFieldValue("maRap", value)}
            options={cinemaDetail.map((item) => ({
              label: item.tenCumRap,
              value: item.maCumRap,
            }))}
            placeholder="Vui lòng chọn rạp"
          />
        </Form.Item>

        <Form.Item label="Ngày chiếu giờ chiếu ">
          <DatePicker
            allowClear={false}
            format="DD/MM/YYYY hh:mm:ss"
            showTime
            onOk={ok}
          />
        </Form.Item>
        <Form.Item label="Giá vé">
          <InputNumber
            onChange={(value) => {
              setFieldValue("giaVe", value);
            }}
            min={75000}
            max={150000}
          />
        </Form.Item>
        <Button
          htmlType="submit"
          className="mx-auto block my-5 bg-blue-400 text-white rounded-3xl font-bold"
        >
          XÁC NHẬN
        </Button>
      </Form>
    </AdminDashboard>
  );
};

export default Showtimes;
