import { Input } from "antd";
import { useFormik } from "formik";
import React, { Fragment } from "react";
import { EyeInvisibleOutlined, EyeTwoTone } from "@ant-design/icons";
import SignupCustomize from "./style.module.css";
import Header from "../../components/Header";
import { dangKy } from "../../services/QuanLyNguoiDung";
import * as yup from "yup";
import { openPopUpModal } from "../../components/PopupModal/signinPopup";
import Swal from "sweetalert2";
import { useHistory } from "react-router-dom";
import { useTranslation } from "react-i18next";

const schema = yup.object().shape({
  taiKhoan: yup.string().required("Vui lòng nhập tài khoản"),
  matKhau: yup.string().required("Vui lòng nhập mật khẩu"),
  email: yup
    .string()
    .required("Vui lòng nhập email")
    .email("Email không hợp lệ"),
  soDt: yup
    .string()
    .required("Vui lòng nhập số đt")
    .matches(/^[0-9]+$/g, "Số ĐT phải là số"),
  hoTen: yup.string().required("Vui lòng nhập họ và tên"),
});

const Signup = () => {
  const {
    handleBlur,
    handleChange,
    touched,
    errors,
    setTouched,
    isValid,
    values,
  } = useFormik({
    initialValues: {
      taiKhoan: "",
      matKhau: "",
      email: "",
      soDt: "",
      hoTen: "",
    },
    validationSchema: schema,
    validateOnMount: true,
  });

  const alertSuccess = () => {
    Swal.fire("Tạo tài khoản thành công!", "Nhấn để đăng nhập", "success").then(
      (result) => {
        if (result.isConfirmed) {
          openPopUpModal();
        }
      }
    );
  };

  const history = useHistory();
  const { t } = useTranslation();

  const handleSubmit = async (e) => {
    e.preventDefault();

    // set tất cả input touched = true hết khi submit
    setTouched({
      taiKhoan: true,
      matKhau: true,
      email: true,
      soDt: true,
      hoTen: true,
    });
    
    if (!isValid) return;

    try {
      const res = await dangKy(values);
      console.log(res);
      history.push("/");
      alertSuccess();
    } catch (err) {
      console.log("SignUp", err);
    }
  };

  const backToHome = async () => {
    await history.push("/");
    openPopUpModal();
  };

  return (
    <Fragment>
      <Header />
      <div
        class="h-screen bg-center"
        style={{
          backgroundImage: `url(${"https://tix.vn/app/assets/img/icons/bg2.jpg"})`,
          background: "cover",
        }}
      >
        <div className="flex h-full ">
          <div
            className={`${SignupCustomize["modal"]} md:w-1/3 lg:w-1/3 sm:w-1/3`}
          >
            <h3 className="text-2xl font-bold text-center text-white">
              {t("Đăng kí tài khoản")}
            </h3>

            <form onSubmit={handleSubmit}>
              <div className="mt-2">
                <div>
                  <label className="block">
                    <p className="mb-1">{t("Tài khoản")}</p>
                    <Input
                      onBlur={handleBlur} 
                      onChange={handleChange}
                      name="taiKhoan"
                    />
                    {touched.taiKhoan && (
                      <p className="text-red-700">{errors.taiKhoan}</p>
                    )}
                  </label>
                </div>
                <div className="mt-2">
                  <label className="block">
                    <p className="mb-1">{t("Mật Khẩu")}</p>

                    <Input.Password
                      className={SignupCustomize["input"]}
                      name="matKhau"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      iconRender={(visible) =>
                        visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />
                      }
                    />
                    {touched.matKhau && (
                      <p className="text-red-700">{errors.matKhau}</p>
                    )}
                  </label>
                </div>

                <div className="mt-2">
                  <label className="block">
                    <p className="mb-1">Email</p>
                    <Input
                      onBlur={handleBlur}
                      onChange={handleChange}
                      name="email"
                      className={SignupCustomize["input"]}
                    />
                    {touched.email && (
                      <p className="text-red-700">{errors.email}</p>
                    )}
                  </label>
                </div>
                <div className="mt-2">
                  <label className="block">
                    <p className="mb-1">{t("Số điện thoại")}</p>
                    <Input
                      onBlur={handleBlur}
                      onChange={handleChange}
                      name="soDt"
                      className={SignupCustomize["input"]}
                    />
                    {touched.soDt && (
                      <p className="text-red-700">{errors.soDt}</p>
                    )}
                  </label>
                </div>
                <div className="mt-2">
                  <label className="block">
                    <p className="mb-1">{t("Họ và tên")}</p>
                    <Input
                      onBlur={handleBlur}
                      onChange={handleChange}
                      name="hoTen"
                      className={SignupCustomize["input"]}
                    />
                    {touched.hoTen && (
                      <p className="text-red-700">{errors.hoTen}</p>
                    )}
                  </label>
                </div>

                <div className="flex justify-center">
                  <button
                    type="submit"
                    className={`${SignupCustomize["button-submit"]} mt-6 `}
                  >
                    {t("Tạo tài khoản")}
                  </button>
                </div>
                <div className="mt-6 text-grey-dark text-center">
                  {t("Bạn đã có tài khoản?")}
                  <button
                    onClick={backToHome}
                    className={`${SignupCustomize["navLink"]} pl-2`}
                  >
                    {t("Đăng nhập")}
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default Signup;
