import React, { Fragment, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, useParams } from "react-router";
import Button from "../../components/Buttons";
import { createAction } from "../../store/actions";
import { fetchBooking, ticketPurchage } from "../../store/actions/cinema";
import { actionType } from "../../store/actions/type";
import style from "./style.module.css";
import classNames from "classnames";
import Swal from "sweetalert2";
import { Tabs } from "antd";
import {
  PreLoaderCallApi,
  openPreLoaderCallApi,
  closedPreLoaderCallApi,
} from "../../components/PreLoader/CallApi";
import { useTranslation } from "react-i18next";

const Booking = (props) => {
  const { id } = useParams();

  const dispatch = useDispatch();

  const history = useHistory();

  const { t } = useTranslation();

  const { ticketMovie, ticketMovieAnotherBooked } = useSelector(
    (state) => state.Cinema
  );

  const { thongTinPhim, danhSachGhe } = useSelector(
    (state) => state.Cinema.bookingMovie
  );

  // vé xếp theo thứ tự tăng dần
  ticketMovie.sort(function (a, b) {
    if (a.stt < b.stt) {
      return -1;
    }
    if (a.stt > b.stt) {
      return 1;
    }
    return 0;
  });

  ticketMovie.sort(function (a, b) {
    return a.stt.length - b.stt.length;
  });

  // let myarray = ["12", "14", "100", "101",'33','1000','1003'];
  // myarray.sort(function (a, b) {
  //   if (a < b) {
  //     return -1;
  //   }
  //   if (a > b) {
  //     return 1;
  //   }
  //   return 0;
  // });
  // console.log(myarray);
  // myarray.sort(function (a, b) {
  //   return a.length - b.length;
  // });
  // console.log(myarray);

  useEffect(() => {
    window.scrollTo(0, 0);
    openPreLoaderCallApi();
    dispatch(fetchBooking(id, closedPreLoaderCallApi));
  }, [dispatch, id]);

  const handleBooking = (ticket) => {
    dispatch(createAction(actionType.SET_TICKET, ticket));
  };

  const handlePurchage = () => {
    if (ticketMovie.length === 0) {
      alert(`${t("Vui lòng chọn ghế trước khi thanh toán")}`);
      return;
    }
    const ticketInfo = {
      maLichChieu: 0,
      danhSachVe: [],
    };

    ticketInfo.maLichChieu = id;
    ticketInfo.danhSachVe = ticketMovie;
    Alert(ticketInfo);
  };

  const setKey = props.setKey;

  const Alert = (ticketInfo) => {
    Swal.fire({
      position: "center",
      icon: "question",
      title: `${t("Xác nhận mua vé")}`,
      showConfirmButton: true,
      showDenyButton: true,
      confirmButtonText: `${t("Xác nhận")}`,
      denyButtonText: `${t("Chọn lại chỗ ngồi")}`,
    }).then((res) => {
      if (res.isConfirmed) {
        dispatch(ticketPurchage(ticketInfo));
        setKey("2");
      }
      if (res.isDenied) {
        return;
      }
    });
  };

  const backToHome = async () => {
    await history.goBack();
    dispatch(createAction(actionType.SET_CLEAR_BOOKING));
  };

  // const { tenPhim, tenCumRap, ngayChieu, gioChieu, tenRap } = thongTinPhim;

  const content = useSelector((state) => state.Me?.content);

  return (
    <div className="container min-h-screen">
      <div className="grid grid-cols-12">
        <div className="col-span-9 mt-7">
          <div className="bg-black w-5/6 h-2 mx-auto"></div>
          <div className={`${style["trapezoid"]} mx-auto text-center`}>
            <h3 className="pt-3 text-white">{t("Màn hình")}</h3>
          </div>
          <div className="w-5/6 mx-auto text-center">
            <PreLoaderCallApi />
            {/* Nếu chưa get data ghế xong thì null, rồi thì hiện */}
            {danhSachGhe
              ? danhSachGhe.map((item, index) => {
                  const { maGhe, loaiGhe, stt, daDat } = item;
                  let isBooking = ticketMovie.findIndex(
                    (ticket) => ticket.maGhe === maGhe
                  );

                  // check xem ghế có bị đặt cùng lúc bởi người khác ko
                  let anotherBooking = ticketMovieAnotherBooked.findIndex(
                    (ticket) => ticket.maGhe === maGhe
                  );

                  const stateSeat = classNames({
                    [style.seat]: true,
                    [style.seatBooked]: daDat === true,
                    [style.seatVip]: loaiGhe === "Vip",
                    [style.seatBooking]: isBooking !== -1,
                    [style.anotherBooked]: anotherBooking !== -1,
                  });

                  return (
                    <Fragment key={maGhe}>
                      {/* Chia làm 2 state đã đặt và chưa đặt và render ra ghế */}

                      {daDat ? (
                        <button disabled="true" className={stateSeat}>
                          <ion-icon name="close-outline"></ion-icon>
                        </button>
                      ) : anotherBooking !== -1 ? (
                        // khách khác đã book vé
                        <button disabled="true" className={stateSeat}>
                          <ion-icon
                            size="small"
                            name="ticket-outline"
                          ></ion-icon>
                        </button>
                      ) : (
                        // vé còn trống
                        <button
                          onClick={() => handleBooking(item)}
                          className={stateSeat}
                        >
                          <p className="text-center leading-8">{stt}</p>
                        </button>
                      )}
                      {(index + 1) % 16 === 0 ? <br /> : ""}
                    </Fragment>
                  );
                })
              : null}
          </div>
          <div className="mt-5 flex justify-center mx-auto w-2/3">
            <table className="table-auto">
              <thead>
                <tr>
                  <th className="p-2">{t("Ghế chưa đặt")}</th>
                  <th className="p-2">{t("Ghế đang đặt")}</th>
                  <th className="p-2">{t("Ghế vip")}</th>
                  <th className="p-2">{t("Ghế đã được đặt")}</th>
                  <th className="p-2">{t("Ghế khách đặt")}</th>
                </tr>
              </thead>
              <tbody className="divide-y divide-gray-200 text-center">
                <tr>
                  <td>
                    <button className={`${style["seat"]}`}>00</button>
                  </td>
                  <td>
                    <button
                      className={`${style["seatBooking"]} ${style["seat"]}`}
                    >
                      00
                    </button>
                  </td>
                  <td>
                    <button className={`${style["seat"]} ${style["seatVip"]}`}>
                      00
                    </button>
                  </td>
                  <td>
                    <button
                      className={`${style["seat"]} ${style["seatBooked"]}`}
                    >
                      <ion-icon size="small" name="close-outline"></ion-icon>
                    </button>
                  </td>
                  <td>
                    <button
                      className={`${style["seat"]} ${style["anotherBooked"]}`}
                    >
                      <ion-icon
                        className={style["ion-icon"]}
                        size="small"
                        name="ticket-outline"
                      ></ion-icon>
                    </button>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        <div className="col-span-3 min-h-screen">
          <img
            className="block w-full h-1/3"
            src={thongTinPhim?.hinhAnh}
            atl={thongTinPhim?.tenPhim}
          />

          <hr />
          <p className="p-1">
            <span className="font-semibold">{t("Địa điểm")}:</span>{" "}
            {thongTinPhim?.tenCumRap}
          </p>
          <p className="p-1">
            <span className="font-semibold">{t("Ngày chiếu")}:</span>{" "}
            {thongTinPhim?.ngayChieu}
          </p>
          <p className="p-1">
            <span className="font-semibold">{t("Giờ chiếu")}:</span>{" "}
            {thongTinPhim?.gioChieu}
          </p>
          <hr />
          <div className="flex my-2">
            <div className="">
              <span className="text-red-400 font-bold">{t("Ghế")}:</span>
            </div>
            <div>
              {ticketMovie.map((item, index) => {
                return (
                  <Fragment key={item.maGhe}>
                    <span className="p-1 text-green-600 font-bold">
                      [{item.tenGhe}]
                    </span>
                    {(index + 1) % 10 === 0 ? <br /> : ""}
                  </Fragment>
                );
              })}
            </div>
          </div>

          <hr />
          <div className="my-1">
            <i className="font-semibold">Email</i> <br />
            {content?.email}
          </div>
          {content?.soDT ? (
            <div className="my-1">
              <i className="font-semibold">Phone</i> <br />
              {content?.soDt}
            </div>
          ) : null}

          <hr />
          <p className="font-semibold my-1">
            {t("Tổng tiền")}:
            <span className="text-green-400 text-center text-3xl font-bold ml-2">
              {ticketMovie
                .reduce((total, ticket) => {
                  return (total += ticket.giaVe);
                }, 0)
                .toLocaleString()}{" "}
              đ
            </span>{" "}
          </p>

          <div className="mb-0 mt-10 flex flex-col justify-end w-3/4 mx-auto">
            <div onClick={handlePurchage}>
              <Button></Button>
            </div>
            <button
              onClick={backToHome}
              className="mt-2 bg-red-500 text-white rounded-xl h-7 opacity-50 hover:opacity-100"
            >
              {t("QUAY LẠI")}
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

const { TabPane } = Tabs;

const TabsBooking = () => {
  const ticketMovie = useSelector((state) => state.Cinema.ticketMovie);
  const { thongTinPhim } = useSelector((state) => state.Cinema.bookingMovie);

  const history = useHistory();
  const dispatch = useDispatch();
  const { t } = useTranslation();

  // User click đặt vé xong sẽ chuyển tab = key
  const [key, setKey] = useState("1");

  const backToHome = async () => {
    await history.push("/");
    dispatch(createAction(actionType.SET_CLEAR_BOOKING));
  };

  return (
    <div className="p-5">
      <Tabs activeKey={key} defaultActiveKey="1">
        <TabPane tab={`${t("B1: CHỌN GHẾ & THANH TOÁN")}`} key="1">
          <Booking setKey={setKey} />
        </TabPane>
        <TabPane tab={`${t("B2: KẾT QUẢ ĐẶT VÉ")}`} key="2">
          <div className="container">
            <div className="bg-gray-200 p-6">
              <h1 className="font-bold text-red-700 text-center pb-5">
                {t("VUI LÒNG KIỂM TRA THÔNG TIN ĐẶT VÉ")}
              </h1>
              <div className="bg-white rounded-md shadow-md flex justify-center w-1/2 mx-auto p-6">
                <img
                  src={thongTinPhim?.hinhAnh}
                  alt="123"
                  className="object-cover object-center w-1/2 pr-2 rounded-md h-auto"
                />
                <div className="">
                  <h2 className="text-xl font-bold tracking-wide">
                    {ticketMovie?.tenPhim}
                  </h2>
                  <div>
                    <p className="font-extrabold text-green-700 ">
                      {thongTinPhim?.tenCumRap}
                    </p>
                    <p className="font-extrabold text-green-700 ">
                      {thongTinPhim?.diaChi}
                    </p>
                    <p className="font-bold">
                      {t("Ngày chiếu")}:{" "}
                      <span className="font-extrabold text-green-700 ">
                        {thongTinPhim?.ngayChieu}
                      </span>
                    </p>
                    <p className="font-bold">
                      {t("Suất chiếu")}:{" "}
                      <span className="font-extrabold text-green-700 ">
                        {thongTinPhim?.gioChieu}
                      </span>
                    </p>
                    <p className="font-bold">
                      {t("Thông tin rạp")}:{" "}
                      <span className="font-extrabold text-green-700 ">
                        {thongTinPhim?.tenRap}
                      </span>
                    </p>
                    <p className="font-bold">
                      {t("Tên phim")}:{" "}
                      <span className="font-extrabold text-green-700 ">
                        {thongTinPhim?.tenPhim}
                      </span>
                    </p>
                  </div>

                  <div>
                    <p className="font-bold">{t("Ghế đã đặt")}:</p>
                    {ticketMovie?.map((item) => {
                      const { maGhe, tenGhe, loaiGhe, giaVe } = item;
                      return (
                        <div
                          key={maGhe}
                          className="flex overflow-hidden rounded-lg bg-gray-300 my-4"
                        >
                          <div className="flex items-center justify-center px-2 bg-indigo-400 text-coolGray-800 w-1/5">
                            <p className="text-white text-2xl font-bold">
                              {tenGhe}
                            </p>
                          </div>
                          <div className="flex items-center justify-between flex-1 p-3">
                            <p className="text-2xl font-semibold">{loaiGhe}</p>
                            <p>{giaVe.toLocaleString()}</p>
                          </div>
                        </div>
                      );
                    })}
                  </div>
                  <div className="mx-auto text-center">
                    <button
                      onClick={backToHome}
                      className="bg-blue-400 opacity-50 hover:opacity-100 rounded p-2 text-white text-lg"
                    >
                      {t("Quay về trang chủ")}
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </TabPane>
      </Tabs>
    </div>
  );
};

export default TabsBooking;
