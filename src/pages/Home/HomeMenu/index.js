import React from "react";
import CinemaGroup from "../../../components/CinemaGroup";

const HomeMenu = () => {
  return (
    <div className="bg-gray-200 p-5">
      <div
        style={{ height: "auto", marginBottom: 70, width: "70vw" }}
        className="container mx-auto bg-white rounded-xl"
      >
        <div className="content p-2">
          <CinemaGroup />
        </div>
      </div>
    </div>
  );
};

export default HomeMenu;
