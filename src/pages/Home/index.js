import Layout from "../../HOCs/Layout";
import React, { useEffect } from "react";
import HomeCarousel from "../../components/HomeCarousel";
import HomeMenu from "./HomeMenu";
import { useDispatch } from "react-redux";
import { fetchMovieList } from "../../store/actions/movielist";
import MultipleRows from "../../components/MultipleRows";
import { createAction } from "../../store/actions";
import { actionType } from "../../store/actions/type";
import { fetchMe } from "../../store/actions/auth";

const Home = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchMovieList());
  });

  const token = localStorage.getItem("token");
  useEffect(() => {
    if (token) {
      dispatch(createAction(actionType.SET_ISLOGIN));
      dispatch(fetchMe);
      console.log("token", token);
    }
  }, [dispatch, token]);

  return (
    <Layout>
      <div className="mx-auto">
        <HomeCarousel />
        <MultipleRows />
        <HomeMenu />
      </div>
    </Layout>
  );
};

export default Home;
