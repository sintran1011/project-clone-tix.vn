import React, { useEffect, useState } from "react";
import Layout from "../../HOCs/Layout";
import { CustomCard } from "@tsamantanis/react-glassmorphism";
import "@tsamantanis/react-glassmorphism/dist/index.css";
import style from "./style.module.css";
import "./style.css";
import Button from "../../components/Buttons";
import { useDispatch, useSelector } from "react-redux";
import { fetchDetail } from "../../store/actions/movielist";
import { Divider, Progress } from "antd";
import TrailerModal from "../../components/TrailerModal";
import { Tabs } from "antd";
import moment from "moment";
import { useHistory, useParams } from "react-router-dom";
import { openPopUpModal } from "../../components/PopupModal/signinPopup";
import { useTranslation } from "react-i18next";

const { TabPane } = Tabs;

const Detail = () => {
  const dispatch = useDispatch();
  const { id } = useParams();
  const { t } = useTranslation();
  useEffect(() => {
    window.scrollTo(0, 0);

    dispatch(fetchDetail(id));
  }, [dispatch, id]);

  const {
    tenPhim,
    trailer,
    hinhAnh,
    ngayKhoiChieu,
    danhGia,
    moTa,
    heThongRapChieu,
  } = useSelector((state) => state.Cinema.detailMovie);

  const [isOpen, setOpen] = useState(false);

  const openTrailerModal = () => {
    setOpen(!isOpen);
  };

  const scrollTo = () => {
    document.querySelector("#booking").scrollIntoView({ behavior: "smooth" });
  };

  const history = useHistory();

  const booking = (maLichChieu) => {
    const token = localStorage.getItem("token");

    if (token) {
      history.push(`/Booking/${maLichChieu}`);
    }
    openPopUpModal();
  };

  return (
    <Layout>
      <TrailerModal
        modalState={isOpen}
        openModal={openTrailerModal}
        trailer={trailer}
      />
      <div
        style={{
          backgroundImage: `url(${hinhAnh})`,
          minHeight: "100vh",
          backgroundRepeat: "no-repeat",
          backgroundPosition: "center",
          backgroundSize: "cover",
        }}
      >
        <CustomCard
          style={{ minHeight: "200vh" }}
          effectColor="blue" // required
          color="#fff" // default color is white
          blur={10} // default blur value is 10px
          borderRadius={"none"} // default border radius value is 10px
        >
          <div className="grid grid-cols-12 justify-between mt-44 w-3/4 mx-auto">
            <div style={{ height: "50vh" }} className="col-span-8 flex">
              <div className="content w-2/5 relative rounded overflow-hidden">
                <div className="trailer">
                  <i
                    onClick={openTrailerModal}
                    class="material-icons header-icon cursor-pointer"
                  >
                    play_circle
                  </i>
                </div>
                <img className="h-full w-full " src={hinhAnh} alt={tenPhim} />
              </div>
              <div className="pl-5 w-3/5 my-auto">
                <p>{moment(ngayKhoiChieu).format("DD-MM-YYYY")}</p>
                <h1 className="text-4xl font-bold mb-1 text-transparent bg-clip-text bg-gradient-to-br to-pink-400 from-red-600">
                  {tenPhim}
                </h1>
                <p>
                  {moTa?.length > 380 ? (
                    <span>{moTa.slice(0, 380)}...</span>
                  ) : (
                    moTa
                  )}
                </p>
                <p>{t("Thời lượng")}: 120p</p>
                <div onClick={scrollTo} className="w-1/2 mt-5">
                  <Button></Button>
                </div>
              </div>
            </div>
            <div className="col-span-3 col-start-10 my-auto">
              <div className="mx-0"></div>
              <div class="flex flex-col justify-center items-center">
                <Progress
                  type="circle"
                  strokeColor={{
                    "0%": "#108ee9",
                    "100%": "#87d068",
                  }}
                  percent={danhGia * 10}
                  format={(percent) => `${percent / 10}`}
                />

                <div class="flex items-center mt-4 mb-4">
                  <svg
                    class="mx-1 w-4 h-4 fill-current text-yellow-500"
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 20 20"
                  >
                    <path d="M10 15l-5.878 3.09 1.123-6.545L.489 6.91l6.572-.955L10 0l2.939 5.955 6.572.955-4.756 4.635 1.123 6.545z" />
                  </svg>
                  <svg
                    class="mx-1 w-4 h-4 fill-current text-yellow-500"
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 20 20"
                  >
                    <path d="M10 15l-5.878 3.09 1.123-6.545L.489 6.91l6.572-.955L10 0l2.939 5.955 6.572.955-4.756 4.635 1.123 6.545z" />
                  </svg>
                  <svg
                    class="mx-1 w-4 h-4 fill-current text-yellow-500"
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 20 20"
                  >
                    <path d="M10 15l-5.878 3.09 1.123-6.545L.489 6.91l6.572-.955L10 0l2.939 5.955 6.572.955-4.756 4.635 1.123 6.545z" />
                  </svg>
                  <svg
                    class="mx-1 w-4 h-4 fill-current text-yellow-500"
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 20 20"
                  >
                    <path d="M10 15l-5.878 3.09 1.123-6.545L.489 6.91l6.572-.955L10 0l2.939 5.955 6.572.955-4.756 4.635 1.123 6.545z" />
                  </svg>
                  <svg
                    class="mx-1 w-4 h-4 fill-current text-gray-400"
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 20 20"
                  >
                    <path d="M10 15l-5.878 3.09 1.123-6.545L.489 6.91l6.572-.955L10 0l2.939 5.955 6.572.955-4.756 4.635 1.123 6.545z" />
                  </svg>
                </div>
                <p className="text-3xl text-white">{t("Đánh giá")}</p>
              </div>
            </div>
          </div>
          <div
            id="booking"
            className="container mx-auto bg-white rounded mt-60"
            style={{ width: "70vw", minHeight: "50vh" }}
          >
            <Tabs
              className={style["ant-tabs-ink-bar::after"]}
              style={{ minHeight: "50vh" }}
              defaultActiveKey="1"
            >
              <TabPane
                tab={<span style={{ fontSize: 25 }}>{t("Lịch Chiếu")}</span>}
                key="1"
              >
                <Tabs tabPosition="left" style={{ minHeight: "50vh" }}>
                  {heThongRapChieu?.map((item) => {
                    const { maHeThongRap, tenHeThongRap, logo, cumRapChieu } =
                      item;
                    return (
                      <TabPane
                        key={maHeThongRap}
                        tab={
                          <div>
                            <div
                              style={{ width: "18vw" }}
                              className="flex flex-row items-center w-max py-4 px-4 hover:opacity-100 "
                            >
                              <img
                                className="block rounded-lg mr-2"
                                src={logo}
                                style={{ width: 50, height: 50 }}
                                alt={tenHeThongRap}
                              />

                              <h1 className="text-xl">{tenHeThongRap}</h1>
                            </div>
                            <Divider
                              orientation="center"
                              className={`${style["marginDivider"]} bg-black bg-opacity-5`}
                            ></Divider>
                          </div>
                        }
                      >
                        {cumRapChieu?.map((item) => {
                          const {
                            maCumRap,
                            tenCumRap,
                            hinhAnh,
                            diaChi,
                            lichChieuPhim,
                          } = item;
                          return (
                            <div key={maCumRap}>
                              <div className="flex p-4 mt-0 ">
                                <img
                                  className="block"
                                  src={hinhAnh}
                                  alt="123"
                                  style={{ width: 50, height: 50 }}
                                />
                                <div className="pl-2">
                                  <h1>{tenCumRap}</h1>
                                  <p>{diaChi}</p>
                                </div>
                              </div>
                              <div className="grid grid-cols-4 gap-4">
                                {lichChieuPhim?.map((item) => {
                                  const {
                                    maLichChieu,
                                    maRap,
                                    // tenRap,
                                    ngayChieuGioChieu,
                                    // giaVe,
                                    // thoiLuong,
                                  } = item;

                                  return (
                                    <button
                                      type="button"
                                      onClick={() => booking(maLichChieu)}
                                      key={maRap}
                                      className="col-span-1 text-green-600 font-bold pl-4"
                                    >
                                      <div className="bg-gray-200 rounded text-center mb-1 p-2 hover:bg-red-400 hover:text-white">
                                        {moment(ngayChieuGioChieu).format(
                                          "hh:mm A"
                                        )}
                                      </div>
                                    </button>
                                  );
                                })}
                              </div>
                              <Divider />
                            </div>
                          );
                        })}
                      </TabPane>
                    );
                  })}
                </Tabs>
              </TabPane>
              <TabPane
                tab={<span style={{ fontSize: 25 }}>{t("Thông tin")}</span>}
                key="2"
              >
                Content of Thông tin
              </TabPane>
              <TabPane
                tab={<span style={{ fontSize: 25 }}>{t("Đánh giá")}</span>}
                key="3"
              >
                Content of Đánh giá
              </TabPane>
            </Tabs>
          </div>
        </CustomCard>
      </div>
    </Layout>
  );
};

export default Detail;
