import { Input } from "antd";
import { useFormik } from "formik";
import React, { Fragment } from "react";
import { NavLink, useHistory, useLocation } from "react-router-dom";
import {
  EyeInvisibleOutlined,
  EyeTwoTone,
  UserOutlined,
} from "@ant-design/icons";
import SigninCustomize from "./style.module.css";
import { useDispatch } from "react-redux";
import { signIn } from "../../store/actions/auth";
import { outsideClosePopupModal } from "../../components/PopupModal/signinPopup";
import Swal from "sweetalert2";
import * as yup from "yup";
import { useTranslation } from "react-i18next";

const schema = yup.object().shape({
  taiKhoan: yup.string().required("Vui lòng nhập tài khoản"),
  matKhau: yup.string().required("Vui lòng nhập mật khẩu"),
});

const Signin = () => {
  const formik = useFormik({
    initialValues: {
      taiKhoan: "",
      matKhau: "",
    },
    validationSchema: schema,
    validateOnMount: true,
  });

  const dispatch = useDispatch();
  const history = useHistory();
  const location = useLocation();
  const { t } = useTranslation();

  const handleSubmit = async (e) => {
    e.preventDefault();
    formik.setTouched({
      taiKhoan: true,
      matKhau: true,
    });
    if (!formik.isValid) {
      alert("Vui lòng nhập tài khoản");
    } else {
      dispatch(signIn(formik.values, outsideClosePopupModal, Alert));
      // Alert.fire({
      //   icon: "success",
      //   title: "Signed in successfully",
      // });

      // Quay lại trang trước nếu user click sign in ngay trên trang sign up
      if (location.pathname === "/Signup") {
        history.goBack();
      }
    }
  };

  // set guest
  const handleSetDefaultUser = () => {
    const guestUser = {
      taiKhoan: "123@admin",
      matKhau: "Tramn",
    };

    formik.setValues({
      taiKhoan: guestUser.taiKhoan,
      matKhau: guestUser.matKhau,
    });
  };

  // alert success set props
  const Alert = Swal.mixin({
    toast: true,
    position: "center",
    showConfirmButton: false,
    timer: 1800,
    timerProgressBar: true,
  });

  return (
    <Fragment>
      <div className={SigninCustomize["content"]}>
        <form onSubmit={handleSubmit}>
          <div>
            <span className="text-sm text-white">
              {t("Chào mừng quay trở lại")}
            </span>
            <h1 className="text-2xl text-white font-bold">
              {t("Đăng nhập vào tài khoản")}
            </h1>
          </div>
          <div className="mt-5">
            <label className={SigninCustomize["label"]}>
              {t("Tên đăng nhập")}
            </label>
            <Input
              value={formik.values.taiKhoan}
              onChange={formik.handleChange}
              name="taiKhoan"
              type="text"
              className={SigninCustomize["input"]}
              placeholder={t("Vui lòng nhập tên đăng nhập")}
              prefix={<UserOutlined />}
            />
          </div>
          <div className="my-3">
            <label className={SigninCustomize["label"]}>{t("Mật khẩu")}</label>
            <Input.Password
              className={SigninCustomize["input"]}
              name="matKhau"
              value={formik.values.matKhau}
              onChange={formik.handleChange}
              iconRender={(visible) =>
                visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />
              }
              placeholder={t("Vui lòng nhập mật khẩu")}
            />
          </div>
          <div className="flex justify-between">
            <div>
              <input
                className="cursor-pointer"
                type="radio"
                name="rememberMe"
              />
              <span className="text-sm text-white pl-1">
                {t("Lưu mật khẩu")}
              </span>
            </div>
            <NavLink
              className={`${SigninCustomize["NavLink"]} pr-2`}
              to="/Signup"
            >
              {t("Quên tài khoản?")}
            </NavLink>
          </div>

          <button
            type="submit"
            className={`${SigninCustomize["Button-submit"]} mt-4 mb-3`}
          >
            {t("Đăng nhập ngay")}
          </button>

          <button
            className={SigninCustomize["Button-guest"]}
            type="button"
            onClick={handleSetDefaultUser}
          >
            {t("Tài khoản khách")}
          </button>
        </form>
        <p className="mt-8 text-center text-white">
          {t("Chưa có tài khoản?")}
          <NavLink
            className={`${SigninCustomize["NavLink"]} pl-2`}
            to="/Signup"
          >
            <span className="text-blue-400 hover:text-blue-900">
              {t("Đăng kí ngay hôm nay")}
            </span>
          </NavLink>
        </p>
      </div>
    </Fragment>
  );
};

export default Signin;
