import { createAction } from ".";
import { actionType } from "./type";
import { dangNhap } from "../../services/QuanLyNguoiDung";
import { thongTinTaiKhoan } from "../../services/QuanLyNguoiDung";

export const signIn = (userData, closedTopup, Alert) => {
  return async (dispatch) => {
    try {
      const res = await dangNhap(userData);
      dispatch(createAction(actionType.SET_ME, res.data));
      console.log(res);
      if (res.data.statusCode === 200) {
        Alert.fire({
          icon: "success",
          title: "Signed in successfully",
        });
        dispatch(createAction(actionType.SET_ISLOGIN));
        localStorage.setItem("token", res.data.content.accessToken);
        closedTopup();
      }
    } catch (err) {
      console.log("SignIn", err.response.data.content);
      alert(err.response.data.content);
    }
  };
};

export const fetchMe = async (dispatch) => {
  try {
    const res = await thongTinTaiKhoan();
    if (res.data.statusCode === 200) {
      dispatch(createAction(actionType.SET_ME, res.data));
      console.log(res.data.content);
    }
  } catch (err) {
    console.log("fetchMe", { ...err });
  }
};
