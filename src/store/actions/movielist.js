import { actionType } from "./type";
import { createAction } from ".";
import {
  capNhatPhimUpload,
  layDanhSachPhim,
  layThongTinLichChieuPhim,
  layThongTinPhim,
  xoaPhim,
} from "../../services/QuanLyPhim";
import { message } from "antd";

export const fetchMovieList = (tenphim) => {
  return async (dispatch) => {
    try {
      const res = await layDanhSachPhim(tenphim);
      dispatch(createAction(actionType.SET_MOVIELIST, res.data));
      console.log("MovieList", res.data);
    } catch (err) {
      console.log("MovieList", err);
    }
  };
};

export const fetchDetail = (id) => {
  return async (dispatch) => {
    try {
      const res = await layThongTinLichChieuPhim(id);
      dispatch(createAction(actionType.SET_DETAIL, res.data));
      console.log("Detail", res.data);
    } catch (err) {
      console.log("Detail", err);
    }
  };
};

// admin

export const fetchInfoFilm = (id) => {
  return async (dispatch) => {
    try {
      const res = await layThongTinPhim(id);
      if (res.status === 200) {
        dispatch(createAction(actionType.ADMIN_GET_INFO_FILM, res.data));
        console.log("InfoFilm", res.data.content);
      }
    } catch (err) {
      console.log("InfoFilm", { ...err });
    }
  };
};

export const deleteFilm = (id) => {
  return async (dispatch) => {
    try {
      const res = await xoaPhim(id);
      console.log("Delete", res.data);
      message.info("Xóa phim thành công");
      await dispatch(fetchMovieList());
    } catch (err) {
      console.log("Delele", { ...err });
    }
  };
};

export const editFilm = (formData) => {
  return async (dispatch) => {
    try {
      const res = await capNhatPhimUpload(formData);
      console.log("EditFilm",res?.data.content)
      message.success("Cập nhật phim thành công");
      dispatch(fetchMovieList())
    } catch (err) {
      console.log("UpdateFilm", { ...err });
    }
  };
};
