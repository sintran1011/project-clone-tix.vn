// import axios from "axios";
import { actionType } from "./type";
import { createAction } from ".";
import { layDanhSachBanner } from "../../services/QuanLyPhim";

export const fetchBanner = () => {
  return async (dispatch) => {
    try {
      const res = await layDanhSachBanner();
      dispatch(createAction(actionType.SET_BANNER, res.data));
      console.log("banner", res.data);
    } catch (err) {
      console.log("banner", err);
    }
  };
};
