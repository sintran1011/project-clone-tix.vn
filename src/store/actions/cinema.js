import { createAction } from ".";
import { actionType } from "./type";
import {
  LayThongTinCumRapTheoHeThong,
  layThongTinHeThongRap,
  taoLichChieu,
} from "../../services/QuanLyRap";
import { layDanhSachPhongVe, datVe } from "../../services/QuanLyDatVe";
import { message } from "antd";

export const fetchCinema = () => {
  return async (dispatch) => {
    try {
      const res = await layThongTinHeThongRap();
      if (res.status === 200) {
        dispatch(createAction(actionType.SET_CINEMA_GROUP, res.data));
      }

      console.log("Cinema", res.data);
    } catch (err) {
      console.log("Cinema", err);
    }
  };
};

export const fetchCinemaList = (id) => {
  return async (dispatch) => {
    try {
      const res = await LayThongTinCumRapTheoHeThong(id);
      if (res.status === 200) {
        dispatch(createAction(actionType.SET_CINEMA_DETAIL, res.data));
      }
      console.log("CinemaList", res.data);
    } catch (err) {
      console.log("CinemaList", { ...err });
    }
  };
};

export const fetchBooking = (id, closedPreLoaderCallApi) => {
  return async (dispatch) => {
    try {
      const res = await layDanhSachPhongVe(id);
      if (res.status === 200) {
        dispatch(createAction(actionType.SET_BOOKING, res.data));
        closedPreLoaderCallApi();
      }

      console.log("Booking", res.data);
    } catch (err) {
      console.log("Booking", err);
    }
  };
};

export const ticketPurchage = (ticketInfo) => {
  return async () => {
    try {
      const res = await datVe(ticketInfo);
      if (res.status === 200) {
        console.log("buyingTicket", res.data.content);
      }
    } catch (err) {
      console.log("ticketPurchage", { ...err });
    }
  };
};

export const makeShowtime = (showtimeInfo) => {
  return async () => {
    try {
      const res = await taoLichChieu(showtimeInfo);

      console.log("makeShowtime", res.data.content);
      message.success("Tạo lịch chiếu thành công");
    } catch (err) {
      alert("makeShowtime", { ...err });
    }
  };
};
