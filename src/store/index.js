import { createStore, combineReducers, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";
import Carousel from "./reducers/Carousel";
import MovieList from "./reducers/MovieList";
import Cinema from "./reducers/Cinema";
import Me from "./reducers/Me";
import AccountState from "./reducers/AccountState";

const rootReducer = combineReducers({
  Carousel,
  MovieList,
  Cinema,
  Me,
  AccountState,
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
  rootReducer,
  composeEnhancers(applyMiddleware(thunk))
);

export default store;
