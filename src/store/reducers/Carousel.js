import { actionType } from "../actions/type";

const initialState = {
  banner: [],
};

const reducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case actionType.SET_BANNER:
      state.banner = payload.content;
      return { ...state };
    default:
      return state;
  }
};

export default reducer;
