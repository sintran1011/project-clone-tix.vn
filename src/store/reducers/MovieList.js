import { actionType } from "../actions/type";

const initialState = {
  moviesList: [],
  dangChieu: true,
  sapChieu: true,
  cloneMoviesList: [],
  infoFilm: "",
};

const reducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case actionType.SET_MOVIELIST:
      state.moviesList = payload.content;
      state.cloneMoviesList = state.moviesList;
      return { ...state };

    case actionType.SET_NOW_SHOWING:
      state.moviesList = state.cloneMoviesList.filter(
        (movie) => movie.dangChieu === state.dangChieu
      );
      return { ...state };

    case actionType.SET_COMING_SOON:
      state.moviesList = state.cloneMoviesList.filter(
        (movie) => movie.sapChieu === state.sapChieu
      );
      return { ...state };

    case actionType.ADMIN_GET_INFO_FILM:
      state.infoFilm = payload.content;
      return { ...state };

    default:
      return state;
  }
};

export default reducer;
