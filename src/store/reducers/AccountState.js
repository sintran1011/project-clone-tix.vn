import { actionType } from "../actions/type";

const initialState = {
  login: false,
};

const reducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case actionType.SET_ISLOGIN:
      state.login = true;
      return { ...state };

    case actionType.SET_ISLOGOUT:
      state.login = false;
      return { ...state };

    default:
      return { ...state };
  }
};

export default reducer;
