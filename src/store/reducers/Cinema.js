import { actionType } from "../actions/type";

const initialState = {
  cinemaGroup: [],
  cinemaDetail: [],
  detailMovie: [],
  bookingMovie: [],
  ticketMovie: [],
  ticketMovieAnotherBooked: [{ maGhe: 120200 }],
  ticketBookedList: [],
};

const reducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case actionType.SET_CINEMA_GROUP:
      state.cinemaGroup = payload.content;
      return { ...state };

    case actionType.SET_CINEMA_DETAIL:
      state.cinemaDetail = payload.content;
      return { ...state };

    case actionType.SET_DETAIL:
      state.detailMovie = payload.content;
      return { ...state };

    case actionType.SET_BOOKING:
      state.bookingMovie = payload.content;
      return { ...state };

    case actionType.SET_CLEAR_BOOKING:
      state.bookingMovie = [];
      return { ...state };

    case actionType.SET_TICKET:
      let cloneTicketMovie = [...state.ticketMovie];

      let index = cloneTicketMovie.findIndex(
        (item) => item.maGhe === payload.maGhe
      );

      if (index === -1) {
        cloneTicketMovie.push(payload);
      } else {
        cloneTicketMovie.splice(index, 1);
      }

      state.ticketMovie = cloneTicketMovie;

      return { ...state };

    default:
      return state;
  }
};

export default reducer;
