export const engFlag = () => {
  return (
    <svg
      className="inline pr-1"
      xmlns="http://www.w3.org/2000/svg"
      width="20"
      height="25"
      viewBox="0 0 640 480"
    >
      <defs>
        <clipPath id="a">
          <path fill-opacity=".67" d="M-85.333 0h682.67v512h-682.67z" />
        </clipPath>
      </defs>
      <g clip-path="url(#a)" transform="translate(80) scale(.94)">
        <g stroke-width="1pt">
          <path fill="#006" d="M-256 0H768.02v512.01H-256z" />
          <path
            fill="#fff"
            d="M-256 0v57.244l909.535 454.768H768.02V454.77L-141.515 0H-256zM768.02 0v57.243L-141.515 512.01H-256v-57.243L653.535 0H768.02z"
          />
          <path
            fill="#fff"
            d="M170.675 0v512.01h170.67V0h-170.67zM-256 170.67v170.67H768.02V170.67H-256z"
          />
          <path
            fill="#c00"
            d="M-256 204.804v102.402H768.02V204.804H-256zM204.81 0v512.01h102.4V0h-102.4zM-256 512.01L85.34 341.34h76.324l-341.34 170.67H-256zM-256 0L85.34 170.67H9.016L-256 38.164V0zm606.356 170.67L691.696 0h76.324L426.68 170.67h-76.324zM768.02 512.01L426.68 341.34h76.324L768.02 473.848v38.162z"
          />
        </g>
      </g>
    </svg>
  );
};

export const japFlag = () => {
  return (
    <svg
      className="inline pr-1"
      xmlns="http://www.w3.org/2000/svg"
      width="20"
      height="25"
      viewBox="0 0 640 480"
    >
      <defs>
        <clipPath id="a">
          <path fill-opacity=".67" d="M-88.001 32h640v480h-640z" />
        </clipPath>
      </defs>
      <g
        fill-rule="evenodd"
        stroke-width="1pt"
        clip-path="url(#a)"
        transform="translate(88.001 -32)"
      >
        <path fill="#fff" d="M-128 32h720v480h-720z" />
        <circle
          cx="523.08"
          cy="344.05"
          r="194.93"
          fill="#d30000"
          transform="translate(-168.44 8.618) scale(.76554)"
        />
      </g>
    </svg>
  );
};

export const vietFlag = () => {
  return (
    <svg
      className="inline pr-1"
      xmlns="http://www.w3.org/2000/svg"
      width="20"
      height="25"
      viewBox="0 0 640 480"
    >
      <defs>
        <clipPath id="a">
          <path fill-opacity=".67" d="M-85.334 0h682.67v512h-682.67z" />
        </clipPath>
      </defs>
      <g
        fill-rule="evenodd"
        clip-path="url(#a)"
        transform="translate(80.001) scale(.9375)"
      >
        <path fill="#ec0015" d="M-128 0h768v512h-768z" />
        <path
          fill="#ff0"
          d="M349.59 381.05l-89.576-66.893-89.137 67.55 33.152-109.77-88.973-67.784 110.08-.945 34.142-109.44 34.873 109.19 110.08.144-88.517 68.423 33.884 109.53z"
        />
      </g>
    </svg>
  );
};
