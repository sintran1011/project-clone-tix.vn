import React, { Fragment, useState } from "react";
import { NavLink } from "react-router-dom";
import Button from "../Buttons/index";
import "./style.css";
import TrailerModal from "../TrailerModal";
import { useTranslation } from "react-i18next";

const MovieList = (props) => {
  const { tenPhim, trailer, hinhAnh, ngayKhoiChieu, maPhim } =
    props.movieDetail;

  const [isOpen, setOpen] = useState(false);
  const { t } = useTranslation();

  const openTrailerModal = () => {
    setOpen(!isOpen);
  };

  return (
    <Fragment>
      <TrailerModal
        modalState={isOpen}
        openModal={openTrailerModal}
        trailer={trailer}
      />
      <div className="movie-card mt-0">
        <div
          className="movie-header"
          style={{
            backgroundImage: `url(${hinhAnh})`,
            backgroundSize: "cover",
            backgroundRepeat: "no-repeat",
            backgroundPosition: "center",
          }}
        >
          <div className="overlay-movieCard">
            <div className="header-icon-container">
              <i
                onClick={openTrailerModal}
                class="material-icons header-icon block m-auto"
              >
                play_circle
              </i>
            </div>
          </div>
        </div>
        {/*movie-header*/}
        <div className="movie-content pt-2">
          <div
            style={{ minHeight: "110px" }}
            className="rounded hidden movie-content-button bg-opacity-0"
          >
            <NavLink to={`/Detail/${maPhim}`}>
              <Button />
            </NavLink>
          </div>
          <div
            style={{ minHeight: "110px" }}
            className="movie-content-header p-2"
          >
            <h3 className="h-8 mb-0 text-2xl font-bold text-transparent bg-clip-text bg-gradient-to-br to-pink-400 from-red-600">
              {tenPhim.length > 20 ? tenPhim.slice(0, 20) : tenPhim}
            </h3>

            <div className="movie-info pb-3 ">
              <div className="info-section align-middle">
                <label className="m-0 align-top">{t("Ngày khởi chiếu")}</label>
                <span>{ngayKhoiChieu}</span>
              </div>

              <div className="info-section align-middle"></div>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default MovieList;
