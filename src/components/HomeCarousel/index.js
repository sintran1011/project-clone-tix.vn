import React, { useEffect } from "react";
import { Carousel } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { fetchBanner } from "../../store/actions/carousel";
import "./style.css";

const HomeCarousel = () => {
  const { banner } = useSelector((state) => state.Carousel);

  const contentStyle = {
    height: "600px",
    color: "#fff",
    lineHeight: "160px",
    textAlign: "center",
    backgroundPosition: "center",
    backgroundSize: "100%",
    backgroundRepeat: "no-repeat",
  };

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchBanner());
  }, [dispatch]);

  return (
    <div>
      <Carousel autoplay className="z-0" effect="fade">
        {banner.map((item) => {
          return (
            <div key={item.maBanner}>
              <div
                style={{
                  ...contentStyle,
                  backgroundImage: `url(${item.hinhAnh})`,
                }}
              ></div>
            </div>
          );
        })}
      </Carousel>
      ,
    </div>
  );
};

export default HomeCarousel;
