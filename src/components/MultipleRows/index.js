import React from "react";
import Slider from "react-slick";
import styleSlick from "./style.module.css";
import MovieList from "../MovieList";
import { useDispatch, useSelector } from "react-redux";
import { createAction } from "../../store/actions";
import { actionType } from "../../store/actions/type";
import { useTranslation } from "react-i18next";

function SampleNextArrow(props) {
  const { className, style, onClick } = props;
  return (
    <div
      className={`${className} ${styleSlick["slick-prev"]}`}
      style={{ ...style, display: "block" }}
      onClick={onClick}
    />
  );
}

function SamplePrevArrow(props) {
  const { className, style, onClick } = props;
  return (
    <div
      className={`${className} ${styleSlick["slick-next"]}`}
      style={{ ...style, display: "block", left: "-50px" }}
      onClick={onClick}
    />
  );
}

const MultipleRows = () => {
  const settings = {
    className: "center ",
    centerMode: true,
    infinite: true,
    centerPadding: "60px",
    slidesToShow: 3,
    speed: 500,
    rows: 2,
    slidesPerRow: 1,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
    autoplay: true,
  };

  const { moviesList } = useSelector((state) => state.MovieList);

  const dispatch = useDispatch();

  const { t } = useTranslation();

  const nowShowing = () => {
    dispatch(createAction(actionType.SET_NOW_SHOWING));
  };

  const comingSoon = () => {
    dispatch(createAction(actionType.SET_COMING_SOON));
  };

  return (
    <div className="py-10">
      <div className="text-center pb-5">
        <button
          onClick={nowShowing}
          type="button"
          className={styleSlick["button"]}
        >
          {t("PHIM ĐANG CHIẾU")}
        </button>
        <button
          onClick={comingSoon}
          type="button"
          className={styleSlick["button"]}
        >
          {t("PHIM SẮP CHIẾU")}
          <span className={styleSlick["new"]}>New</span>
        </button>
      </div>

      <Slider style={{ width: "75%", margin: "auto" }} {...settings}>
        {moviesList?.slice(0, 24).map((item) => {
          const { maPhim } = item;

          return (
            <div className={`${styleSlick[`width-item`]}`} key={maPhim}>
              <MovieList movieDetail={item} />
            </div>
          );
        })}
      </Slider>
    </div>
  );
};

export default MultipleRows;
