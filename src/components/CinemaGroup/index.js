import React, { useEffect } from "react";
import { Tabs, Divider } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { fetchCinema } from "../../store/actions/cinema";
import tabCustomize from "./style.module.css";
import { NavLink, useHistory } from "react-router-dom";
import moment from "moment";
import { openPopUpModal } from "../PopupModal/signinPopup";
import { useTranslation } from "react-i18next";

const { TabPane } = Tabs;

const CinemaList = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const { t } = useTranslation();

  useEffect(() => {
    dispatch(fetchCinema());
  }, [dispatch]);

  const booking = (maLichChieu) => {
    const token = localStorage.getItem("token");

    if (token) {
      history.push(`/Booking/${maLichChieu}`);
    }
    openPopUpModal();
  };

  const { cinemaGroup } = useSelector((state) => state.Cinema);
  console.log(cinemaGroup);
  return (
    <Tabs tabPosition={"left"}>
      {cinemaGroup?.map((cinema) => {
        const { maHeThongRap, tenHeThongRap, logo, lstCumRap } = cinema;
        return (
          <TabPane
            className={tabCustomize["padding"]}
            key={maHeThongRap}
            tab={
              <div>
                <img
                  src={logo}
                  atl={tenHeThongRap}
                  className="rounded-full w-12 block my-3 mr-2"
                />
              </div>
            }
          >
            <Tabs className={tabCustomize["max-height"]} tabPosition="left">
              {lstCumRap?.map((cumRap) => {
                const { maCumRap, tenCumRap, hinhAnh, diaChi, danhSachPhim } =
                  cumRap;
                // fetch hệ thống rạp
                return (
                  <TabPane
                    className={tabCustomize["padding-y"]}
                    key={maCumRap}
                    tab={
                      <div className=" mt-0" style={{ width: "22vw" }}>
                        <div className="flex">
                          <img
                            src={hinhAnh}
                            atl="123"
                            style={{
                              width: "50px",
                              height: "100%",
                              display: "block",
                              verticalAlign: "middle",
                            }}
                          />

                          <div className=" pl-2 text-left">
                            <h2 className="text-pink-600 mb-0">{tenCumRap}</h2>
                            <label>
                              {diaChi.length > 40 ? (
                                <span>{diaChi.slice(0, 40)}...</span>
                              ) : (
                                diaChi
                              )}
                            </label>
                            <p className="mb-0">[{t("Chi tiết")}]</p>
                          </div>
                        </div>
                        <div>
                          <Divider
                            orientation="center"
                            className={`${tabCustomize["marginDivider"]} bg-black bg-opacity-5`}
                          ></Divider>
                        </div>
                      </div>
                    }
                  >
                    <div
                      className={`${tabCustomize["scrollbar"]} ${tabCustomize["divide"]} scrollbar-thin scrollbar-thumb-indigo-400 scrollbar-track-indigo-100 scrollbar-thumb-rounded-2xl`}
                    >
                      {/* fetch danh sách phim theo rạp */}
                      {danhSachPhim?.slice(0, 15).map((movieList) => {
                        const {
                          maPhim,
                          tenPhim,
                          hinhAnh,
                          lstLichChieuTheoPhim,
                        } = movieList;
                        return (
                          <div key={maPhim} className="flex py-2">
                            <img
                              src={hinhAnh}
                              atl={tenPhim}
                              style={{
                                width: "80px",
                                height: "100px",
                                display: "block",
                                verticalAlign: "middle",
                              }}
                            />

                            <div className=" pl-2 text-left">
                              <h2 className="text-blue-800 mb-0">{tenPhim}</h2>
                              <label>
                                {diaChi.length > 40 ? (
                                  <span>{diaChi.slice(0, 40)}...</span>
                                ) : (
                                  diaChi
                                )}
                              </label>
                              {/* fetch lịch chiếu theo phim */}
                              <div className="grid grid-cols-3 gap-3 pr-5">
                                {lstLichChieuTheoPhim
                                  ?.slice(0, 10)
                                  .map((item) => {
                                    const { maLichChieu, ngayChieuGioChieu } =
                                      item;
                                    return (
                                      <button
                                        className="text-2xl rounded bg-green-500 p-2 hover:text-white"
                                        onClick={() => booking(maLichChieu)}
                                        key={maLichChieu}
                                      >
                                        {moment(ngayChieuGioChieu).format(
                                          "hh:mm A"
                                        )}
                                      </button>
                                    );
                                  })}
                              </div>
                            </div>
                          </div>
                        );
                      })}
                    </div>
                  </TabPane>
                );
              })}
            </Tabs>
          </TabPane>
        );
      })}
    </Tabs>
  );
};

export default CinemaList;
