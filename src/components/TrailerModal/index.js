import React from "react";
import { Modal } from "react-responsive-modal";
import "react-responsive-modal/styles.css";
import "./style.css";

const TrailerModal = (props) => {
  const { modalState, openModal, trailer } = props;
  const closedButton = () => {
    return (
      <img
        src="https://cdn-icons.flaticon.com/png/512/1621/premium/1621173.png?token=exp=1640662104~hmac=f172af921e2a4cf95559f58190560b7e"
        style={{ width: 40, height: 40 }}
        atl="trailer"
      />
    );
  };

  return (
    <Modal
      classNames="m-0"
      open={modalState}
      onClose={() => {
        openModal();
      }}
      closeIcon={closedButton()}
      center
    >
      <iframe
        width="100%"
        height="100%"
        src={trailer}
        title="YouTube video player"
        frameborder="2"
        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
        allowfullscreen
      ></iframe>
    </Modal>
  );
};

export default TrailerModal;
