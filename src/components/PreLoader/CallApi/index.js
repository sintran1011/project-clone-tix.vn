import React from "react";
import Style from "./style.module.css";

export const PreLoaderCallApi = () => {
  return (
    <div id="LoaderAPI" className={Style["content"]}>
      <div className={`${Style["loader"]} ${Style["loader-7"]}`}>
        <div className={`${Style["line"]} ${Style["line1"]}`} />
        <div className={`${Style["line"]} ${Style["line2"]}`} />
        <div className={`${Style["line"]} ${Style["line3"]}`} />
      </div>
    </div>
  );
};

export const openPreLoaderCallApi = () => {
  document.querySelector("#LoaderAPI").classList.add(`${Style["open"]}`);
};

export const closedPreLoaderCallApi = () => {
  document.querySelector("#LoaderAPI").classList.remove(`${Style["open"]}`);
};

