import React from "react";
import style from "./style.module.css";

const PreLoader = () => {
  return (
    <div className={`${style["body"]} min-h-screen`}>
      <div className={style["loader"]}>
        <div className={style["dot"]} />
        <div className={style["dot"]} />
        <div className={style["dot"]} />
        <div className={style["dot"]} />
        <div className={style["dot"]} />
        <div className={style["dot"]} />
        <div className={style["dot"]} />
        <div className={style["dot"]} />
        <div className={style["dot"]} />
        <div className={style["dot"]} />
        <div className={style["dot"]} />
        <div className={style["dot"]} />
        <div className={style["dot"]} />
        <div className={style["dot"]} />
        <div className={style["dot"]} />
        <div className={style["dot"]} />
        <div className={style["dot"]} />
        <div className={style["dot"]} />
        <div className={style["dot"]} />
        <div className={style["dot"]} />
        <div className={style["dot"]} />
        <div className={style["dot"]} />
        <div className={style["dot"]} />
        <div className={style["dot"]} />
        <div className={style["dot"]} />
        <div className={style["dot"]} />
      </div>
    </div>
  );
};

export default PreLoader;
