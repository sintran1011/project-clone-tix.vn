import React, { Fragment, useEffect } from "react";
import { NavLink, useHistory } from "react-router-dom";
import style from "./style.module.css";
import { PopupModal, openPopUpModal } from "../PopupModal/signinPopup";
import { t } from "i18next";
import { useSelector } from "react-redux";
import { message } from "antd";

const MenuBar = () => {
  useEffect(() => {
    const list = document.querySelectorAll(".list");
    function activeLink() {
      list.forEach((item) => item.classList.remove(`${style["active"]}`));
      this.classList.add(`${style["active"]}`);
    }
    list.forEach((item) => {
      item.addEventListener("mouseover", activeLink);
    });
  }, []);

  const user = useSelector((state) => state.Me);
  const history = useHistory();

  const adminAuth = () => {
    history.push("/Admin");
  };

  return (
    <Fragment>
      <div className={style["navigation"]}>
        <ul id="id1">
          <li className={`${style["active"]} list`}>
            <div className="card">
              <NavLink className={`${style["navlink"]}`} to="/">
                <span className={style["icon"]}>
                  <ion-icon name="home-outline"></ion-icon>
                </span>
                <span className={style["text"]}>{t("Trang chủ")}</span>
              </NavLink>
            </div>
          </li>

          <li className="list">
            <div className="card">
              <NavLink className={`${style["navlink"]}`} to="/Contact">
                <span className={style["icon"]}>
                  <ion-icon name="chatbubbles-outline"></ion-icon>
                </span>
                <span className={style["text"]}>{t("Liên hệ")}</span>
              </NavLink>
            </div>
          </li>

          <li className="list">
            <div className="card">
              <a href="#" className={`${style["navlink"]}`} onClick={adminAuth}>
                <span className={style["icon"]}>
                  <ion-icon name="information-circle-outline"></ion-icon>
                </span>
                <span className={style["text"]}>{t("Quản trị")}</span>
              </a>
            </div>
          </li>

          <li className="list">
            <a
              className={`${style["navlink"]}`}
              onClick={() => openPopUpModal(user)}
            >
              <span className={style["icon"]}>
                <ion-icon name="person-circle-outline"></ion-icon>
              </span>
              <span className={style["text"]}>{t("Đăng nhập")}</span>
            </a>
          </li>

          <li className="list">
            <NavLink className={`${style["navlink"]}`} to="/Signup">
              <span className={style["icon"]}>
                <ion-icon name="person-add-outline"></ion-icon>
              </span>
              <span className={style["text"]}>{t("Đăng ký")}</span>
            </NavLink>
          </li>

          <div className={style["indicator"]}></div>
        </ul>
      </div>
      <PopupModal />
    </Fragment>
  );
};

export default MenuBar;
