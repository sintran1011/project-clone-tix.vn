import { message, Spin } from "antd";
import React from "react";
import { useTranslation } from "react-i18next";
import { useDispatch, useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { createAction } from "../../store/actions";
import { actionType } from "../../store/actions/type";
import MenuBar from "../MenuBar";
import DropdownLanguage from "./dropdown";

const Header = () => {
  const Me = useSelector((state) => state.Me);
  const { login } = useSelector((state) => state.AccountState);
  console.log(login);

  const { t } = useTranslation();

  const dispatch = useDispatch();

  const userLogin = () => {
    if (login) {
      if (Me === null) {
        return <Spin className="mr-14" size="large" tip="Loading..." />;
      } else {
        return (
          <p className="m-0 pr-6">
            {t("Xin chào ,")}
            <p className="text-indigo-400 font-bold">{Me.content.hoTen}</p>{" "}
            <button
              onClick={() => {
                localStorage.removeItem("token");
                dispatch(createAction(actionType.SET_ISLOGOUT));
                message.info("Bạn đã đăng xuất");
              }}
              className="mx-auto block text-red-600 text-opacity-50 hover:text-opacity-100"
            >
              [
              <span className="pl-1 align-middle">
                <ion-icon name="log-out-outline"></ion-icon>
              </span>{" "}
              <span>{t("Đăng xuất")} </span> ]
            </button>
          </p>
        );
      }
    } else {
      return;
    }
  };

  return (
    <header
      style={{ backgroundColor: "#222327" }}
      className="p-4 text-white text-xl h-28 sticky top-0 w-full z-10 "
    >
      <div className="flex justify-between h-auto mx-auto items-center">
        <NavLink
          to="/"
          aria-label="Back to homepage"
          className="flex items-center p-2 w-1/5"
        >
          <img
            src="https://cyberlearn.vn/wp-content/uploads/2020/03/cyberlearn-min-new-opt2.png"
            atl="description of image"
          />
        </NavLink>
        <MenuBar />

        <div className="w-1/5 flex align-baseline justify-between">
          {userLogin()}
          <DropdownLanguage />
        </div>
      </div>
    </header>
  );
};

export default Header;
