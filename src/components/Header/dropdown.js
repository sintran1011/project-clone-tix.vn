import React, { useEffect, useState } from "react";
import { Menu, Dropdown, Button, Space } from "antd";
import { Fragment } from "react";
import { useTranslation } from "react-i18next";
import { engFlag, vietFlag, japFlag } from "../Flag";

const DropdownLanguage = () => {
  const [language, setLanguage] = useState("Tiếng Việt");

  const { i18n } = useTranslation();

  useEffect(() => {
    i18n.changeLanguage("Vie");
  }, []);

  const flag = () => {
    if (language === "Tiếng Việt") {
      return vietFlag();
    } else if (language === "日本") {
      return japFlag();
    } else {
      return engFlag();
    }
  };

  const handleLanguage = (e) => {
    console.log(e.key);
    i18n.changeLanguage(e.key);
    if (e.key === "Vie") {
      setLanguage("Tiếng Việt");
    } else if (e.key === "Jap") {
      setLanguage("日本");
    } else {
      setLanguage("English");
    }
  };

  const menu = () => {
    return (
      <Menu onClick={handleLanguage}>
        <Menu.Item key={"Eng"} className="hover:bg-red-500 hover:text-white">
          {engFlag()}
          <span className="font-semibold">English</span>
        </Menu.Item>
        <Menu.Item key={"Jap"} className="hover:bg-red-500 hover:text-white">
          {japFlag()}
          <span className="font-semibold">日本</span>
        </Menu.Item>
        <Menu.Item key={"Vie"} className="hover:bg-red-500 hover:text-white">
          {vietFlag()}
          <span className="font-semibold">Tiếng Việt</span>
        </Menu.Item>
      </Menu>
    );
  };

  return (
    <Fragment>
      <Space direction="vertical">
        <Space wrap>
          <Dropdown arrow="true" overlay={menu} placement="bottomLeft">
            <Button className="bg-blue-500 text-white font-bold ">
              {flag()}
              <span>{language}</span>
            </Button>
          </Dropdown>
        </Space>
      </Space>
    </Fragment>
  );
};

export default DropdownLanguage;
