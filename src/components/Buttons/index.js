import React from "react";
import { useTranslation } from "react-i18next";
import "./style.css";

const Button = () => {
  const { t } = useTranslation();

  return <button class="btn-grad rounded">{t("ĐẶT VÉ")}</button>;
};

export default Button;
