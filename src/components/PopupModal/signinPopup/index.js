import { message } from "antd";
import React from "react";

import Signin from "../../../pages/Signin";
import "./style.css";

export const PopupModal = () => {
  return (
    <div>
      <div
        onClick={outsideClosePopupModal}
        id="activePopup-overlay"
        className="popup-overlay"
      />
      <div id="activePopup" className="popup">
        <Signin />
      </div>
    </div>
  );
};

export const outsideClosePopupModal = () => {
  document
    .getElementById("activePopup-overlay")
    .classList.remove("activePopup-overlay");

  document.getElementById("activePopup").classList.remove("activePopup");
};

export const openPopUpModal = (user) => {
  // cách khác để gọi modal:
  // let activeOverlay = document.getElementById("activePopup-overlay");
  // activeOverlay.classList.add("activePopup-overlay");
  // const token = localStorage.getItem("token");
  // if (token) {
  //   alert("Bạn đã đăng nhập vào tài khoản");
  //   return;
  // }
  const token = localStorage.getItem("token");
  if (!token) {
    document
      .getElementById("activePopup-overlay")
      .classList.add("activePopup-overlay");

    document.getElementById("activePopup").classList.add("activePopup");
  }

  if (token && user === null) {
    message.loading("Vui lòng chờ load thông tin người dùng");
    return;
  }
};
