import { message } from "antd";
import React, { Component } from "react";
import { Route, Redirect } from "react-router-dom";

const createRoute = (condition) => {
  return class extends Component {
    render() {
      const { path, component: RouteComponent, redirectPath } = this.props;

      return (
        <Route
          exact
          path={path}
          render={(routeProps) => {
            if (condition()) {
              return <RouteComponent {...routeProps} />;
            }
            message.warning("Vui lòng log out để đăng kí tài khoản mới");
            return <Redirect to={redirectPath} />;
          }}
        ></Route>
      );
    }
  };
};

export const AuthRoute = createRoute(() => !localStorage.getItem("token"));

export const AuthBookingRoute = createRoute(() =>
  localStorage.getItem("token")
);

export const AuthAdminRoute = createRoute(() => localStorage.getItem("token"));
