import { Input } from "antd";
import React, { useEffect, useState } from "react";
import style from "./adminstyle.module.css";
import classNames from "classnames";
import { Content } from "antd/lib/layout/layout";
import { NavLink, useHistory } from "react-router-dom";

const AdminDashboard = (props) => {
  useEffect(() => {
    // Phải đặt trong useEffect để chạy trước 1 lần, sau đó mới có tác dụng, để ở ngoài phải click chuột
    const list = document.querySelectorAll(".list");
    function activeLink() {
      list.forEach((item) => {
        item.classList.remove(`${style["hovered"]}`);
        this.classList.add(`${style["hovered"]}`);
      });
    }

    list.forEach((item) => {
      item.addEventListener("mouseover", activeLink);
    });
  }, []);

  const [active, setActive] = useState(false);
  const [rotate, setRotate] = useState(false);

  const history = useHistory();

  const navigation = classNames({
    [style.navigation]: true,
    [style.active]: active,
  });

  const toggle = classNames({
    [style.toggle]: true,
    [style.rotate]: rotate,
  });

  const content = classNames({
    [style.content]: true,
    [style.active]: active,
  });

  return (
    <div className={style["container"]}>
      <div className={navigation}>
        <ul>
          <div className="w-5/6 h-20 overflow-hidden mx-auto my-5 ">
            <img
              className="bg-left h-full w-full object-scale-cover"
              src="http://picsum.photos/500"
            />
          </div>

          <li className="list py-3">
            <NavLink to="/">
              <span className={style["icon"]}>
                <ion-icon name="home-outline"></ion-icon>
              </span>
              <span className={style["title"]}>Trang chủ</span>
            </NavLink>
          </li>

          <li className="list py-3">
            <a>
              <span className={style["icon"]}>
                <ion-icon name="person-circle-outline"></ion-icon>
              </span>
              <span className={style["title"]}>Quản lý người dùng</span>
            </a>
          </li>

          <li className="list py-3">
            <NavLink to="/Admin/Films">
              <span className={style["icon"]}>
                <ion-icon name="person-circle-outline"></ion-icon>
              </span>
              <span className={style["title"]}>Quản lý phim</span>
            </NavLink>
          </li>

          <li className="list py-3">
            <NavLink to="/Admin/Showtimes">
              <span className={style["icon"]}>
                <ion-icon name="calendar-number-outline"></ion-icon>
              </span>
              <span className={style["title"]}>Lịch chiếu</span>
            </NavLink>
          </li>

          <li className="list py-3">
            <a
              onClick={async () => {
                await localStorage.removeItem("token");
                history.push("/");
              }}
            >
              <span className={style["icon"]}>
                <ion-icon name="log-out-outline"></ion-icon>
              </span>
              <span className={style["title"]}>Đăng xuất</span>
            </a>
          </li>
        </ul>
      </div>

      <div className={content}>
        <div className={style["topbar"]}>
          <div
            onClick={() => {
              setActive(!active);
              setRotate(!rotate);
            }}
            className={toggle}
          >
            <ion-icon name="caret-back-outline"></ion-icon>
          </div>

          <div className={style["search"]}>
            <label>
              <Input placeholder="Bạn tìm kiếm điều chi :v" />
              <ion-icon name="search-outline"></ion-icon>
            </label>
          </div>

          <div className={style["user"]}>
            <img src={`http://picsum.photos/500`} />
          </div>
        </div>
        <Content
          className="min-h-screen"
          style={{
            margin: "24px 16px",
          }}
        >
          {props.children}
        </Content>
      </div>
    </div>
  );
};

export default AdminDashboard;
