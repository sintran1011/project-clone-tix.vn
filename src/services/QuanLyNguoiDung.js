import { baseService } from ".";

class QuanLyNguoiDung extends baseService {
  dangKy = (userData) => {
    return this.post(`api/QuanLyNguoiDung/DangKy`, userData);
  };

  dangNhap = (userData) => {
    return this.post(`api/QuanLyNguoiDung/DangNhap`, userData);
  };

  thongTinTaiKhoan = () => {
    return this.postMe(`api/QuanLyNguoiDung/ThongTinTaiKhoan`);
  };
}

const quanLyNguoiDung = new QuanLyNguoiDung();

export const { dangKy } = quanLyNguoiDung;

export const { dangNhap } = quanLyNguoiDung;

export const { thongTinTaiKhoan } = quanLyNguoiDung;
