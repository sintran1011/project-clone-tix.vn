import { baseService } from ".";

class QuanLyDatVe extends baseService {
  layDanhSachPhongVe = (id) => {
    return this.getWithParams(
      `api/QuanLyDatVe/LayDanhSachPhongVe?MaLichChieu=${id}`
    );
  };

  datVe = (ticketInfo) => {
    return this.post(`api/QuanLyDatVe/DatVe`, ticketInfo);
  };
}

const quanLyDatVe = new QuanLyDatVe();

export const { layDanhSachPhongVe, datVe } = quanLyDatVe;
