import axios from "axios";
import { DOMAIN, TOKENCYBERSOFT } from "../util/config";

export class baseService {
  get = (url) => {
    return axios({
      url: `${DOMAIN}/${url}`,
      method: "GET",
      headers: {
        TokenCybersoft: TOKENCYBERSOFT,
      },
    });
  };

  getWithParams = (url, id) => {
    return axios({
      url: `${DOMAIN}/${url}`,
      method: "GET",
      params: id,
      headers: {
        TokenCybersoft: TOKENCYBERSOFT,
      },
    });
  };

  post = (url, userData) => {
    return axios({
      url: `${DOMAIN}/${url}`,
      method: "POST",
      data: userData,
      headers: {
        Authorization: "Bearer " + localStorage.getItem("token"),
        TokenCybersoft: TOKENCYBERSOFT,
      },
    });
  };

  postMe = (url) => {
    return axios({
      url: `${DOMAIN}/${url}`,
      method: "POST",
      headers: {
        TokenCybersoft: TOKENCYBERSOFT,
        Authorization: "Bearer " + localStorage.getItem("token"),
      },
    });
  };

  delete = (url, id) => {
    return axios({
      url: `${DOMAIN}/${url}`,
      method: "DELETE",
      params: id,
      headers: {
        TokenCybersoft: TOKENCYBERSOFT,
        Authorization: "Bearer " + localStorage.getItem("token"),
      },
    });
  };
}
