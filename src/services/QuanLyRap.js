import { baseService } from ".";
import { GROUPID } from "../util/config";

class QuanLyRap extends baseService {
  layThongTinHeThongRap = () => {
    return this.get(
      `api/QuanLyRap/LayThongTinLichChieuHeThongRap?maNhom=${GROUPID}`
    );
  };

  LayThongTinCumRapTheoHeThong = (id) => {
    return this.getWithParams(
      `api/QuanLyRap/LayThongTinCumRapTheoHeThong?maHeThongRap=${id}`
    );
  };

  taoLichChieu = (showtimeInfo) => {
    return this.post(`api/QuanLyDatVe/TaoLichChieu`, showtimeInfo);
  };
}

const quanLyRap = new QuanLyRap();

export const { layThongTinHeThongRap } = quanLyRap;

export const { LayThongTinCumRapTheoHeThong } = quanLyRap;

export const { taoLichChieu } = quanLyRap;
