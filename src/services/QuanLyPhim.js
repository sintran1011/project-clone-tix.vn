import { baseService } from ".";
import { GROUPID } from "../util/config";

class QuanLyPhim extends baseService {
  layDanhSachBanner = () => {
    return this.get(`/api/QuanLyPhim/LayDanhSachBanner`);
  };

  layDanhSachPhim = (tenphim = "") => {
    if (tenphim.trim() !== "") {
      return this.get(
        `api/QuanLyPhim/LayDanhSachPhim?maNhom=${GROUPID}&tenPhim=${tenphim}`
      );
    }
    return this.get(`api/QuanLyPhim/LayDanhSachPhim?maNhom=${GROUPID}`);
  };

  layThongTinLichChieuPhim = (id) => {
    return this.getWithParams(
      `api/QuanLyRap/LayThongTinLichChieuPhim?MaPhim=${id}`
    );
  };

  themPhimUploadHinh = (formData) => {
    return this.post(`/api/QuanLyPhim/ThemPhimUploadHinh`, formData);
  };

  layThongTinPhim = (id) => {
    return this.getWithParams(`/api/QuanLyPhim/LayThongTinPhim?MaPhim=${id}`);
  };

  capNhatPhimUpload = (formData) => {
    return this.post(`/api/QuanLyPhim/CapNhatPhimUpload`, formData);
  };

  xoaPhim = (id) => {
    return this.delete(`/api/QuanLyPhim/XoaPhim?MaPhim=${id}`);
  };
}

const quanLyPhim = new QuanLyPhim();

export const { layDanhSachBanner } = quanLyPhim;

export const { layDanhSachPhim } = quanLyPhim;

export const { layThongTinLichChieuPhim } = quanLyPhim;

export const { themPhimUploadHinh } = quanLyPhim;

export const { layThongTinPhim } = quanLyPhim;

export const { capNhatPhimUpload } = quanLyPhim;

export const { xoaPhim } = quanLyPhim;
