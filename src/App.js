import "./App.css";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import Signin from "./pages/Signin";
import News from "./pages/News";
import Contact from "./pages/Contact";
import { useDispatch } from "react-redux";
import { fetchMe } from "./store/actions/auth";
import { useEffect, Suspense, lazy } from "react";
import { AuthRoute, AuthBookingRoute, AuthAdminRoute } from "./HOCs/route";
import PreLoader from "./components/PreLoader";
// import MenuBar from "./components/MenuBar";
// import { PreLoaderCallApi } from "./components/PreLoader/CallApi";
import AddNews from "./pages/Admin/Films/AddNews";
import Films from "./pages/Admin/Films";
import AdminPage from "./pages/Admin";
import Edit from "./pages/Admin/Films/Edit";
import { createAction } from "./store/actions";
import { actionType } from "./store/actions/type";
import Showtimes from "./pages/Admin/Showtimes";

// import Detail from "./pages/Detail";
// import Booking from "./pages/Booking";
// import Home from "./pages/Home";
// import Signup from "./pages/Signup";

const Home = lazy(() => import("./pages/Home"));
const Detail = lazy(() => import("./pages/Detail"));
const Booking = lazy(() => import("./pages/Booking"));
const Signup = lazy(() => import("./pages/Signup"));

function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Suspense fallback={<PreLoader />}>
          <AuthRoute path="/Signin" component={Signin} redirectPath="/" />
          <AuthRoute path="/Signup" component={Signup} redirectPath="/" />
          <AuthBookingRoute
            path="/Booking/:id"
            component={Booking}
            redirectPath="/Signup"
          />
          <Route path="/News" component={News} />
          <Route path="/Contact" component={Contact} />
          <Route path="/Detail/:id" exact component={Detail} />
          <Route path="/" exact component={Home} />
          <Route path="/Loader" component={PreLoader} />
          {/* <Route path="/Menubar" component={MenuBar} /> */}
          {/* <Route path="/LoaderApi" component={PreLoaderCallApi} /> */}
          <Route path="/Admin" component={AdminPage} />
          <Route path="/Admin/Films" component={Films} />
          <Route path="/Admin/Films/Showtimes/:id" component={Showtimes} />
          <Route path="/Admin/Films/AddNews" component={AddNews} />
          <Route path="/Admin/Films/Edit/:id" component={Edit} />
        </Suspense>
      </Switch>
    </BrowserRouter>
  );
}

export default App;
